
library(tidyverse)
library(broom)
library(glmnet)

theme_set(theme_bw(base_size = 16))


all_counts <- read_tsv("combined_posterior.txt") %>%
    filter(Abundance > 10)

conditions <- all_counts %>%
    dplyr::select(Construct, Locus) %>%
    distinct()

counts_by_condition <- lapply(seq(1, nrow(conditions)), function(i) {left_join(slice(conditions, i), all_counts_anno)})

## ## for the synthetically generated sequences
## sequence_anno <- read.table("1st-library-synthetic-matrix.txt")
## sequence_anno$Sequence <- row.names(sequence_anno)

## for the genomically sampled sequences
sequence_anno <- read.table("1st-library-endogenous-matrix.txt")
sequence_anno <- sequence_anno %>%
    transmute(
        Sequence = row.names(sequence_anno)
      , Shared = as.numeric(ES & IE)
      , ES = as.numeric(ES & !Shared)
      , IE = as.numeric(IE & !Shared)
      , Tcf_Motif = as.numeric(Tcf_Motif)
      , Enhancer_Marks = as.numeric(Enhancer_Marks)
    )

all_counts_anno <- inner_join(all_counts, sequence_anno, by = "Sequence")

## alpha = 1 for lasso, 0 for ridge, in between to mix
## + 0 in formula as glmnet already adds it's own intercept.
lmBin <- function(dd) {
    y <- with(dd, cbind(b, a))
    f <- formula(a ~ ES + IE + Shared + ES:Tcf_Motif + IE:Tcf_Motif + Shared:Tcf_Motif + ES:Enhancer_Marks + IE:Enhancer_Marks + Shared:Enhancer_Marks)
    # f <- formula(a ~ Tcf7l2 * (Tcf7l2 + Klf + Ets + Fox + Hnf4a + Gata3 + Smad3 + Oct4 + cMyc + Sox3))
    x <- model.matrix(f, dd)
    fit <- cv.glmnet(x, y, alpha = 0, family = "binomial")
    tidy(coef(fit, s=fit$lambda.min))


lm_binomial_fits <- lapply(counts_by_condition, lmBin)
lm_binomial_fits <- Map(function(x, y) {cbind(x, t(replicate(nrow(x), y))) }, lm_binomial_fits, as.data.frame(t(conditions)))
lm_binomial_fits <- Reduce(rbind, lm_binomial_fits)
