data {
  int<lower = 0> N;             // number of counts
  int<lower = 0> x[N];          // DpnII count
  int<lower = 0> y[N];          // DpnI count
}

parameters {
  real<lower = 0> a;            // alpha (i.e. DpnII methylation rate)
  real<lower = 0> b;            // beta (i.e. DpnI methylation rate)
  real<lower = 0> c;            // fixed gamma rate (i.e. sequence abundance)
  real<lower = 0> s;            // DpnII amplification rate
  real<lower = 0> t;            // DpnI amplification rate
  real<lower = 0, upper = 1> l;            // Drop-out poisson contamination
}

model {
  // Expectation is 1 / rate for exponential.
  a ~ exponential(1);
  b ~ exponential(1);
  c ~ exponential(10);
  s ~ exponential(0.1);
  t ~ exponential(0.1);

  for (i in 1:N) {
    // DpnII then DpnI counts

    // used for fixing the distribution across several samples of the same library
    // real c = 0.05;

    target += log_sum_exp(
      poisson_lpmf(x[i] | l) + neg_binomial_lpmf(0 | a, c),
      log_diff_exp(
        neg_binomial_lpmf(x[i] | a, (c / s)),
        neg_binomial_lpmf(x[i] | a, ((c + 1) / s)) + neg_binomial_lpmf(0 | a, c)
      )
    );
    target += log_sum_exp(
      poisson_lpmf(y[i] | l) + neg_binomial_lpmf(0 | b, c),
      log_diff_exp(
        neg_binomial_lpmf(y[i] | b, (c / t)),
        neg_binomial_lpmf(y[i] | b, ((c + 1) / t)) + neg_binomial_lpmf(0 | b, c)
      )
    );
  }
}
