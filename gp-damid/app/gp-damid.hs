{-# LANGUAGE ScopedTypeVariables #-}

-- My code
import GaussianProcess.GaussianProcess
import GaussianProcess.LBFGS

import Kernels.Kernels
import Kernels.Rbf

-- Random functions (mine)
import Utility.List

-- Random functions (library)
import Data.Maybe (fromMaybe)
import Data.List

import Control.Applicative

-- Math stuff
import Numeric.LinearAlgebra
import Numeric.LBFGS.Vector

-- Data structures
import qualified Data.Vector as V
-- For lbfgs optimisation
import qualified Data.Vector.Storable as VS
  
import Text.Printf (printf)
import System.IO


-- IO helper functions
showDouble :: Double -> String
showDouble = printf "%.6f"
  
showVec :: [Double] -> String
showVec = intercalate ", " . map showDouble

printRvec :: String -> [Double] -> IO ()
printRvec a x = do
  let x' = showVec x
  putStrLn (a ++ " <- c(" ++ x' ++ ")")

printList :: Show a => [a] -> IO ()
printList = putStrLn . intercalate "\t" . map show

-- just want the diagonal of the covariance matrix.
printMVN :: MVN -> IO ()
printMVN (mu, sigma) = do
  printList $ toList mu
  printList . toList $ takeDiag sigma
  -- print . length $ toList mu
  -- print . length . toList $ takeDiag sigma
  
saveVectors :: String -> [Vector Double] -> IO ()
saveVectors file_name vec_list = do
  hn <- openFile (output_folder ++ file_name ++ ".txt") WriteMode  
  -- want each row to be a datapoint and columns as samples.
  -- currently the outer list if samples and inner is datapoints.
  hPutStr hn . unlines . map (unwords . map show) . transpose $ map toList vec_list
  hClose hn
  


-- Helper functions
-- no need to square it as it as already the variance instead of standard deviation
addDiagVar :: Maybe (Vector Double) -> (Matrix Double -> Matrix Double)
addDiagVar v = fromMaybe id v'
  where
    v' = fmap ((+) . diag) v
  
-- choose what sort.
flankingMatrix :: [String] -> [String] -> Matrix Double
flankingMatrix fp tp = sum $ map (covMatrixS equalKernel 1.0) ffs
  where
    -- order: tri-, di-, mono-
    -- ffs = [fp, tp]
    -- ffs = [map (drop 1) fp, map (take 2) tp]
    -- ffs = [map (drop 2) fp, map (take 1) tp]
    -- independent tri- then di-
    -- ffs = [map (take 1) fp, map (take 1 . drop 1) fp, map (drop 2) fp, map (take 1) tp, map (take 1 . drop 1) tp, map (drop 2) tp]
    ffs = [map (take 1 . drop 1) fp, map (drop 2) fp, map (take 1) tp, map (take 1 . drop 1) tp]
    -- fuse up and down to get long range interactions.
    -- ffs = zipWith (++) fp tp 
    -- ffs = zipWith (++) (map (drop 1) fp) (map (take 2) tp)
    -- ffs = zipWith (++) (map (drop 2) fp) (map (take 1) tp)


-- All the IO stuff

-- ks is already preformatted (so (kvs :) . (sigmas *))
-- deal only with a single GP (NOT ALL LOCI / CONSTRUCTS)
printGPinfo :: String -> [String] -> [Matrix Double] -> Vector Double -> [(Matrix Double, Matrix Double)] -> IO ()
printGPinfo fn bb ks y ks_samples = do
  let
    cl = chol . trustSym $ sum ks
    -- removes both sampling and isotropic noise
    k = sum $ drop 2 ks

    -- Smoothed estimates
    y_sm = smoothGPchol cl k y

    -- Leave one out cross validation
    y_cv = loocvGPchol cl k y

    -- Leave out backbone cross validation
    bb_i = map length $ group bb
    y_bb = manycvGPchol bb_i cl k y

    -- and get each component separately.
    -- should work with: smoothGPchol cl k' y
    -- if not switch back to: sampleGPchol cl k' k' y
    yks = map (\k' -> smoothGPchol cl k' y) ks
    -- yks = map (\k' -> sampleGPchol cl k' k' y) ks

    -- sampling the relevant components.
    y_samples = map (\(k12, k22) -> sampleGPchol cl k12 k22 y) ks_samples

    -- and the actual predictive probabilities
    lm = logMarginalChol cl y
    lm_cv = sum . toList $ snd y_cv
    lm_bb = sum . toList $ snd y_bb

  -- Save probabilites

  h_s <- openFile (fn ++ "_stats.txt") WriteMode  
  hPutStrLn h_s . unwords $ map show [lm, lm_cv, lm_bb]
  hClose h_s

  -- Save predictive values.

  h_v <- openFile (fn ++ "_values.txt") WriteMode  
  let
    vs = foldr (\(a, b) acc -> a : b : acc) [] yks
    -- vs = foldr (\(a, b) acc -> a : (takeDiag b) : acc) [] yks
    -- add the cross validation predictions too.
    vs' = (fst y_sm) : (snd y_sm) : (fst y_cv) : (fst y_bb) : vs
  -- save each vector as a column
  hPutStr h_v . unlines . map (unwords . map show) . transpose $ map toList vs'
  hClose h_v

  h_samples <- openFile (fn ++ "_samples.txt") WriteMode  
  let
    y_samples' = foldr (\(a, b) acc -> a : (takeDiag b) : acc) [] y_samples
  hPutStr h_samples . unlines . map (unwords . map show) . transpose $ map toList y_samples'
  hClose h_samples


readData :: IO ([[String]], [(Vector R, Vector R)])
readData = do
  -- handle <- openFile (output_folder ++ "GP_new-lib.txt") ReadMode  
  handle <- openFile (output_folder ++ "GP_new-lib_nonorm.txt") ReadMode  
  -- handle <- openFile (output_folder ++ "GP_new-lib_rosa-diff.txt") ReadMode  
  contents <- hGetContents handle  
  -- for checking that overall ordering doesn't matter
  -- ls <- sample . shuffle $ lines contents
  let
    ls = lines contents
    -- ls = take 1000 $ lines contents
    ls' = transpose $ map (concat . map (splitAtElems '_') . words) ls
    (x, yv) = splitAt 5 ls'
    -- just to make the (mean, variance) pairing explicit so I don't mess it up later.
    yv' = map (\[a, b] -> (a, b)) . block 2 . map (vector . map read) $ yv

    -- just for if i change my mind and to shuffle samples again.
    -- ls_s = fst . (`sampleState` mkStdGen 5811) . replicateM 10 $ mapM shuffle yv'

  return (x, yv')

detectTcfOr :: String -> Maybe String
detectTcfOr x
  | fw && (not rv) = Just "Forward"
  | rv && (not fw) = Just "Reverse"
  | otherwise = Nothing
  where
    fw = isInfixOf "CTTTGAT" x
    rv = isInfixOf "ATCAAAG" x 

detectTcfOr' :: String -> String
detectTcfOr' = fromMaybe (error "no single tcf orientation") . detectTcfOr

readThetas :: String -> IO [Double]
readThetas fn = do
  hn_th <- openFile (fn ++ "_stored-thetas.txt") ReadMode  
  theta <- fmap (map read . words) $ hGetContents hn_th

  hPutStrLn stderr $ fn ++ " parameters"
  hPutStrLn stderr $ show theta

  return theta
  
readSigmas :: String -> IO [Double]
readSigmas fn = do
  hn_ss <- openFile (fn ++ "_stored-sigmas.txt") ReadMode  
  sigmas <- fmap (map read . words) $ hGetContents hn_ss

  hPutStrLn stderr $ fn ++ " parameters"
  hPutStrLn stderr $ show sigmas

  return sigmas

readHypers :: String -> IO ([Double], [Double])
readHypers fn = do

  hn_th <- openFile (fn ++ "_stored-thetas.txt") ReadMode  
  theta <- fmap (map read . words) $ hGetContents hn_th

  hn_ss <- openFile (fn ++ "_stored-sigmas.txt") ReadMode  
  sigmas <- fmap (map read . words) $ hGetContents hn_ss

  -- hClose hn_th
  -- hClose hn_ss

  hPutStrLn stderr $ fn ++ " parameters"
  hPutStrLn stderr $ show sigmas
  hPutStrLn stderr $ show theta

  return (sigmas, theta)

writeTheta :: String -> [Double] -> IO ()
writeTheta fn theta = do

  hn_th <- openFile (fn ++ "_stored-thetas.txt") WriteMode  
  hPutStrLn hn_th . unwords . map show $ theta

  hPutStrLn stderr $ fn ++ " parameters"
  hPutStrLn stderr $ show theta

  hClose hn_th

writeSigmas :: String -> [Double] -> IO ()
writeSigmas fn sigmas = do

  hn_ss <- openFile (fn ++ "_stored-sigmas.txt") WriteMode  
  hPutStrLn hn_ss . unwords . map show $ sigmas

  hPutStrLn stderr $ fn ++ " parameters"
  hPutStrLn stderr $ show sigmas

  hClose hn_ss

writeHypers :: String -> ([Double], [Double]) -> IO ()
writeHypers fn (sigmas, theta) = do

  hn_ss <- openFile (fn ++ "_stored-sigmas.txt") WriteMode  
  hPutStrLn hn_ss . unwords . map show $ sigmas

  hn_th <- openFile (fn ++ "_stored-thetas.txt") WriteMode  
  hPutStrLn hn_th . unwords . map show $ theta

  hPutStrLn stderr $ fn ++ " parameters"
  hPutStrLn stderr $ show sigmas
  hPutStrLn stderr $ show theta

  hClose hn_ss
  hClose hn_th
  
  
runGP :: IO ()
runGP = do  

  hPutStrLn stderr output_folder

  -- format the input data nicely
  (xx, yv) <- readData

  let
    -- backbone, sequence, fiveprime flank, threeprime flank, tcf motif position
    [bb, x, fp, tp, xp'] = xx
    xp :: [Int]
    xp = map read xp'

    xo :: [String]
    xo = map detectTcfOr' x

    -- Actual values and sampling noise.
    ys = map fst yv
    kvs = map (diag . snd) yv

  -- set all the global parameters
  let 
    -- overall parameters
    dna_len = 100
    n = length x

  -- make the simple covariance matrices
  let
    kn :: Matrix Double
    kn = ident n

    -- estimates different mean.
    kc :: Matrix Double
    kc = konst 1 (n, n)
  
    kb :: Matrix Double
    kb = covMatrixS equalKernel 1.0 bb

    kf :: Matrix Double
    kf = flankingMatrix fp tp

    kks = [kn, kc, kb, kf]
    sigmas = map (replicate (length kks)) [0.03, 0.3]

  mapM_ (\i -> do
            hPutStrLn stderr (gpLabels !! i)
            let
              fn = output_folder ++ (gpLabels !! i) ++ "_"
              y = ys !! i
              kv = kvs !! i

            -- Optimise the initial sigmas 
            sigmas' <- lbfgsSigmas (kks, Just kv, y) (sigmas !! (i `mod` 2))
            hn_ss <- openFile (fn ++ "base_stored-sigmas.txt") WriteMode  
            hPutStrLn hn_ss . unwords . map show $ sigmas'
            hClose hn_ss

            -- -- Or read already optimised sigmas.
            -- hn_ss <- openFile (fn ++ "base_stored-sigmas.txt") ReadMode  
            -- read_sigmas' <- hGetContents hn_ss
            -- let sigmas' :: [Double] = map read . words $ read_sigmas'

            let
              ks = kv : scaleByVariance sigmas' kks

            printGPinfo (fn ++ "base") bb ks y []

            -- Run one without the flnking nucleotides as a baseline
            sigmas_noflanking <- lbfgsSigmas (take 3 kks, Just kv, y) (take 3 sigmas')
            hn_ss_nf <- openFile (fn ++ "noflanking_stored-sigmas.txt") WriteMode  
            hPutStrLn hn_ss_nf . unwords . map show $ sigmas'
            hClose hn_ss_nf

            printGPinfo (fn ++ "noflanking") bb (kv : scaleByVariance sigmas_noflanking (take 3 kks)) y []

            -- Global rbf signal

            -- rbf for global
            (s, t) <- lbfgsBothSeparate (kks, kv, y, zip (repeat "all") xp, rbfVectorGrad dna_len) (sigmas' ++ [0.3], [10])
            writeHypers (fn ++ "global") (s, t)
            -- (s, t) <- readHypers (fn ++ "global-optim")

            let k_global = covMatrixS (gatedKernel posKernel) (head $ rbfVectorGrad dna_len t) (zip (repeat "all") xp)
            printGPinfo (fn ++ "global") bb (kv : scaleByVariance s (kks ++ [k_global])) y []

            -- orientation
            -- (s', t') <- lbfgsBothSeparate (kks ++ [k_global], kv, y, zip xo xp, rbfVectorGrad dna_len) (s ++ [0.3], [3])
            (s', t') <- lbfgsBothSeparate (kks ++ [k_global], kv, y, zip xo xp, smkernelVectorGrad dna_len) (s ++ [0.3], [(1/ 11), 0.005])
            writeHypers (fn ++ "orient") (s', t')
            -- (s', t') <- readHypers (fn ++ "orient-optim")

            -- let k_orient = covMatrixS (gatedKernel posKernel) (head $ rbfVectorGrad dna_len t') (zip xo xp)
            let k_orient = covMatrixS (gatedKernel posKernel) (head $ smkernelVectorGrad dna_len t') (zip xo xp)
            printGPinfo (fn ++ "orient") bb (kv : scaleByVariance s' (kks ++ [k_global, k_orient])) y []

            -- position
            (s'', t'') <- lbfgsBothSeparate (kks ++ [k_global, k_orient], kv, y, zip bb xp, rbfVectorGrad dna_len) (s' ++ [0.3], [10])
            writeHypers (fn ++ "backbone") (s'', t'')
            -- (s'', t'') <- readHypers (fn ++ "backbone")

            let k_backbone = covMatrixS (gatedKernel posKernel) (head $ rbfVectorGrad dna_len t'') (zip bb xp)
            printGPinfo (fn ++ "backbone") bb (kv : scaleByVariance s'' (kks ++ [k_global, k_orient, k_backbone])) y []

            -- position sin
            -- (s''', t''') <- lbfgsBothSeparate (kks ++ [k_global, k_orient, k_backbone], kv, y, zip bb xp, sinrbfVectorGrad dna_len) (s'' ++ [0.3], [2, 11])
            (s''', t''') <- lbfgsBothSeparate (kks ++ [k_global, k_orient, k_backbone], kv, y, zip bb xp, smkernelVectorGrad dna_len) (s'' ++ [0.3], [(1/11), 0.005])
            writeHypers (fn ++ "backbone-sin") (s''', t''')
            -- (s''', t''') <- readHypers (fn ++ "backbone-sin-optim")

            let k_backbone_sin = covMatrixS (gatedKernel posKernel) (head $ smkernelVectorGrad dna_len t''') (zip bb xp)
            printGPinfo (fn ++ "backbone-sin") bb (kv : scaleByVariance s''' (kks ++ [k_global, k_orient, k_backbone, k_backbone_sin])) y []

            --  -- READ FIXED THETAS AND OPTIMISE SIGMAS
            -- -- rbf for global
            -- t <- readThetas (output_folder ++ "global")
            -- s <- lbfgsSigmas' (kks, kv, y, zip (repeat "all") xp, rbfVectorGrad dna_len) (sigmas' ++ [0.3], t)
            -- writeHypers (fn ++ "global") (s, t)
            -- -- (s, t) <- readHypers (fn ++ "global-optim")

            -- let k_global = covMatrixS (gatedKernel posKernel) (head $ rbfVectorGrad dna_len t) (zip (repeat "all") xp)
            -- printGPinfo (fn ++ "global") bb (kv : scaleByVariance s (kks ++ [k_global])) y []

            -- -- orientation
            -- t' <- readThetas (output_folder ++ "orient")
            -- s' <- lbfgsSigmas' (kks ++ [k_global], kv, y, zip xo xp, smkernelVectorGrad dna_len) (s ++ [0.3], t')
            -- writeHypers (fn ++ "orient") (s', t')
            -- -- (s', t') <- readHypers (fn ++ "orient-optim")

            -- let k_orient = covMatrixS (gatedKernel posKernel) (head $ smkernelVectorGrad dna_len t') (zip xo xp)
            -- printGPinfo (fn ++ "orient") bb (kv : scaleByVariance s' (kks ++ [k_global, k_orient])) y []

            -- -- position
            -- t'' <- readThetas (output_folder ++ "backbone")
            -- s'' <- lbfgsSigmas' (kks ++ [k_global, k_orient], kv, y, zip bb xp, rbfVectorGrad dna_len) (s' ++ [0.3], t'')
            -- writeHypers (fn ++ "backbone") (s'', t'')
            -- -- (s'', t'') <- readHypers (fn ++ "backbone")

            -- let k_backbone = covMatrixS (gatedKernel posKernel) (head $ rbfVectorGrad dna_len t'') (zip bb xp)
            -- printGPinfo (fn ++ "backbone") bb (kv : scaleByVariance s'' (kks ++ [k_global, k_orient, k_backbone])) y []

            -- -- position sin
            -- t''' <- readThetas (output_folder ++ "backbone-sin")
            -- s''' <- lbfgsSigmas' (kks ++ [k_global, k_orient, k_backbone], kv, y, zip bb xp, smkernelVectorGrad dna_len) (s'' ++ [0.3], t''')
            -- writeHypers (fn ++ "backbone-sin") (s''', t''')
            -- -- (s''', t''') <- readHypers (fn ++ "backbone-sin-optim")

            -- let k_backbone_sin = covMatrixS (gatedKernel posKernel) (head $ smkernelVectorGrad dna_len t''') (zip bb xp)
            -- printGPinfo (fn ++ "backbone-sin") bb (kv : scaleByVariance s''' (kks ++ [k_global, k_orient, k_backbone, k_backbone_sin])) y []

            -- Calculate likelihood at all integer periodicities.
            mapM_ (\p -> do
                      let
                        fn' = fn ++ "sin-tcf-2_" ++ show p ++ "_"

                        -- switch xo to bb for tcf gated vs backbone gated
                        k_sin = covMatrixS (gatedKernel posKernel) (sinrbfVector dna_len [2, p]) (zip xo xp)
                        kks_sin = kks ++ [k_sin]

                      sigmas_sin' <- lbfgsSigmas (kks_sin, Just kv, y) $ sigmas' ++ [0.5]

                      -- -- Store the optimised sigmas in case i want to check them later
                      -- hn_ss' <- openFile (fn' ++ "stored-sigmas.txt") WriteMode  
                      -- hPutStrLn hn_ss' . unwords . map show $ sigmas_sin'
                      -- hClose hn_ss'

                      let
                        ks' = kv : scaleByVariance sigmas_sin' kks_sin
                      printGPinfo fn' bb ks' y []

                  ) $ [1..30]

            -- Calculate likelihood for scanning across rbf length scales
            mapM_ (\l -> do
                      let
                        fn' = fn ++ "rbf-tcf_" ++ show l ++ "_"

                        -- switch xo to bb for tcf gated vs backbone gated
                        k_rbf = covMatrixS (gatedKernel posKernel) (rbfVector dna_len [l]) (zip xo xp)
                        kks_rbf = kks ++ [k_rbf]

                      sigmas_rbf' <- lbfgsSigmas (kks_rbf, Just kv, y) $ sigmas' ++ [0.5]

                      -- -- Store the optimised sigmas in case i want to check them later
                      -- hn_ss' <- openFile (fn' ++ "stored-sigmas.txt") WriteMode  
                      -- hPutStrLn hn_ss' . unwords . map show $ sigmas_sin'
                      -- hClose hn_ss'

                      let
                        ks' = kv : scaleByVariance sigmas_rbf' kks_rbf
                      printGPinfo fn' bb ks' y []

                  ) $ [100, 63, 40, 25, 16, 10, 6.3, 4, 2.5, 1.6, 1]


            hPutStrLn stderr "moo"

            -- Rosa DamA126 and TcfDamA126 logit
            ) [1, 3, 5, 7, 9, 11]
 
  hPutStrLn stderr "woo"


gpLabels :: [String]
gpLabels = [a ++ "_" ++ b ++ "_" ++ c | a <- ["Rosa", "uCD8"], b <- ["DamA126", "TcfDamA126", "TcfDamWT"], c <- ["normal", "logit"]]

output_folder = "data/"
  
main :: IO ()
main = do  

  runGP

  hPutStrLn stderr "woo"

