module Utility.List where

import Data.List

-- General list functions
zipList :: [a] -> [a] -> [a]
zipList xs [] = xs
zipList [] ys = ys
zipList (x:xs) (y:ys) = x : y : zipList xs ys

-- Overlapping
chunk :: Int -> [a] -> [[a]]
chunk 0 _ = error "can't chunk into zero sized fragments"
chunk n x = take (length x - n + 1) . map (take n) . tails $ x
  
-- Non-overlapping.
-- Used for group parameters or reshaping lists.
block :: Int -> [a] -> [[a]]
block 0 _  = error "can't block into zero sized fragments"
block n [] = []
block n x  = take n x : (block n (drop n x))

removeElem :: Int -> [a] -> [a]
removeElem i x = a ++ tail b
  where (a, b) = splitAt i x
  
splitAtElem :: Eq t => t -> [t] -> Maybe ([t], [t])
splitAtElem c [] = Nothing
splitAtElem c x = let (a, b) = span (/= c) x in
  case b of [] -> Just (a, [])
            _  -> Just (a, tail b)

splitAtElems :: Eq t => t -> [t] -> [[t]]
splitAtElems c x = unfoldr (splitAtElem c) x

takeIndices :: [Int] -> [a] -> [a]
takeIndices is xs = map (xs !!) is
