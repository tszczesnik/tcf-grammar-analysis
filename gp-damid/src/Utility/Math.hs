module Utility.Math where

import Prelude hiding ((<>))

import Numeric.LinearAlgebra

import qualified Data.Vector.Storable as V

-- Simple functions.
sigmoid :: Floating a => a -> a
sigmoid x = 1 / (1 + exp (-x))

logit :: Floating a => a -> a
logit x = log (x / (1 - x))


-- Linear algebra (HMatrix).
trace :: Matrix Double -> Double
trace = sumElements . takeDiag

covToCor :: Matrix Double -> Matrix Double
covToCor x = x' <> x <> x'
  where
    x' = diag . cmap ((1 /) . sqrt) $ takeDiag x

-- VECTOR EQUIVALENT FOR DIAGBLOCKS.
-- Will ignore anything that exceeds the length.
blockVector :: V.Storable a => [Int] -> Vector a -> [Vector a]
blockVector [] v = []
blockVector (i:is) v
  | V.null v = []
  | otherwise = v' : (blockVector is vs)
    where (v', vs) = V.splitAt i v
  
-- Makes symmetric blocks and only takes the middle ones
toDiagBlocks :: Element t => [Int] -> Matrix t -> [Matrix t]
toDiagBlocks i x = zipWith (!!) (toBlocks i i x) [0..]

-- log of determinant is more useful and stable
cholLogDet :: Matrix Double -> Double
cholLogDet cl = (2 *) . sumElements . log . takeDiag $ cl

-- For situation with vector, so I don't need to transform it each time.
cholSolveV ::  Matrix Double -> Vector Double -> Vector Double
cholSolveV cl y = head . toColumns . cholSolve cl $ asColumn y

cholInverse :: Matrix Double -> Matrix Double
cholInverse cl = cholSolve cl $ ident n
  where
    n = fst $ size cl

-- So I don't have difficulties inverting matrices.
removeNegativeEigenvalues :: Herm Double -> Herm Double
removeNegativeEigenvalues x = trustSym $ eigvec <> diag eigval' <> tr eigvec
  where
    (eigval, eigvec) = eigSH x
    eigval' = cmap (\i -> max i 0) eigval

safeChol :: Herm Double -> Matrix Double
safeChol x = case mbChol x of
  (Just cl) -> cl
  Nothing -> chol $ removeNegativeEigenvalues x

-- For computing symmetric matrices without double calculating k(x,y) and k(y,x).
raggedTri :: (a -> a -> b) -> [a] -> [[b]]
raggedTri k [] = []
raggedTri k (x:xs) = map (k x) (x:xs) : raggedTri k xs

padRagged :: Num a => Int -> [[a]] -> [[a]]
padRagged i [] = []
padRagged i (x:xs) = (replicate i 0 ++ x) : padRagged (i + 1) xs

symFromRag :: [[Double]] -> Matrix Double
symFromRag xs = u + l
  where
    u = fromRows . map vector $ padRagged 0 xs
    -- Drops the diagonal to avoid double counting it.
    l = fromColumns . map vector . padRagged 1 $ map tail xs

splitTri :: Int -> [a] -> [[a]]
splitTri k [] = []
splitTri 1 (x:xs) = [[x]]
splitTri k xs = x : splitTri (k - 1) xs'
  where
    (x, xs') = splitAt k xs
 

symFromRag' :: Int -> [Double] -> Matrix Double
symFromRag' k xs = symFromRag $ splitTri k xs
