module GaussianProcess.LBFGS where

import GaussianProcess.GaussianProcess
  
import Utility.List

import Kernels.Kernels
import Kernels.Rbf

-- Math stuff
import Numeric.LinearAlgebra
import Numeric.LBFGS.Vector

-- Data structures
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS
  
import Data.Maybe (fromMaybe)

import System.IO

-- Necessary functions
scaleByVariance :: [Double] -> [Matrix Double] -> [Matrix Double]
scaleByVariance ss k = zipWith (*) k $ map (scalar . (^2)) ss

-- ALL THE OPTIMISATION FUNCTIONS

-- How to otimise the positional GP + neural net!
-- Armijo and Wolfe 0.9 seem to be best.
defaultLBFGSParameters :: LBFGSParameters
defaultLBFGSParameters = LBFGSParameters Nothing 0 (BacktrackingWolfe 0.9) Nothing

progressFun :: Int -> ProgressFun a
progressFun kmax i ax ag y ex eg s n k is = do
  hPutStrLn stderr ("log marginal: " ++ show y)
  if k < (fromIntegral kmax)
    then return 0
    else return 1

-- ks should k + each of the subcomponents for which there is a sigma.
-- evaluateSigmas :: EvaluateFun ([Matrix Double], Vector Double)
evaluateSigmas :: EvaluateFun ([Matrix Double], Maybe (Matrix Double), Vector Double)
evaluateSigmas (ks, kn, y) t dt n s = do
  t' <- VS.freeze t
  -- hPutStrLn stderr $ show t'
  let
    e = error "cholesky decomposition failed"
    t'' = fmap realToFrac $ VS.toList t'
    -- Note: negate as I want to maximise the marginal probability.
    k = sum $ scaleByVariance t'' ks
    k' = case kn of (Just kn') -> k + kn'
                    Nothing -> k
    (p:dp) = map negate . fromMaybe e $ logMarginalGrad (k':ks) y
    dp' = zipWith (\s ds -> 2 * s * ds) t'' dp
  -- hPutStrLn stderr $ show dp'
  VS.copy dt . VS.fromList $ map realToFrac dp'
  return $ realToFrac p

lbfgsSigmas :: ([Matrix Double], Maybe (Matrix Double), Vector Double) -> [Double] -> IO [Double]
lbfgsSigmas a t = do
  hPutStrLn stderr "Optimising hyperparameters"
  (err, t') <- lbfgs defaultLBFGSParameters evaluateSigmas (progressFun 20) a t
  -- (err, t') <- lbfgs defaultLBFGSParameters evaluateSigmas (progressFun 50) a t
  hPutStrLn stderr $ show err
  return t'

-- optimise the hyperparameters of the gradient matrix.
-- takes the constant matrix (e.g. sum of sampling variance, isotropic variance, constant, flanking, backbone, etc...)
-- sigma should only be for position matrix; the rest should be implicit in the summed matrix (for now I guess)
evaluateHypers :: EvaluateFun (Matrix Double, Vector Double, [(String, Int)], Double, ([Double] -> [V.Vector Double]))
evaluateHypers (k, y, x, sigma, f) t dt n s = do
  t' <- VS.freeze t
  -- hPutStrLn stderr $ show t'
  let
    e = error "cholesky decomposition failed"
    -- all the hyperparameters
    t'' = fmap realToFrac $ VS.toList t'

    -- make the relevant interactinos matrices. i.e. kp, dkp/ks, dkp/dl
    -- simpler to map over a vector for each hyperparameter rather than using the separate covMatrixGrad function.
    (kp:dkps) = map (\t -> covMatrixS (gatedKernel posKernel) t x) $ f t''

    -- need to scale the gradient by the sigma as well. it is the same sigma for each hyperparameter though (since only one kernel / position matrix).
    s' = scalar sigma^2
    dkps' = map (* s') dkps
    k' = k + kp * s'

    -- Note: negate as I want to maximise the marginal probability.
    (p : dps) = map negate . fromMaybe e $ logMarginalGrad (k' : dkps') y

  -- hPutStrLn stderr $ show dp'
  VS.copy dt . VS.fromList $ map realToFrac dps
  return $ realToFrac p

lbfgsHypers :: (Matrix Double, Vector Double, [(String, Int)], Double, [Double] -> [V.Vector Double]) -> [Double] -> IO [Double]
lbfgsHypers a t = do
  hPutStrLn stderr "Optimising hyperparameters"
  (err, t') <- lbfgs defaultLBFGSParameters evaluateHypers (progressFun 50) a t
  hPutStrLn stderr $ show err
  return t'


-- Combine the above two to update both interaction hyperparameters and the contribution of each component.
-- sigmas first.
evaluateBoth :: EvaluateFun ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], ([Double] -> [V.Vector Double]))
evaluateBoth (ks, kv, y, x, f) t dt n s = do
  t' <- VS.freeze t
  -- hPutStrLn stderr $ show t'
  let
    e = error "cholesky decomposition failed"
    -- the [ks] doesn't contain the final positional matrix (so if of length 1 less than the sigmas).
    n' = length ks + 1
    -- all the hyperparameters
    (sigmas, theta) = splitAt n' . fmap realToFrac $ VS.toList t'

    -- make the relevant interactinos matrices. i.e. kp, dkp/ks, dkp/dl
    -- simpler to map over a vector for each hyperparameter rather than using the separate covMatrixGrad function.
    (kp:dkps) = map (\t -> covMatrixS (gatedKernel posKernel) t x) (f theta)

    ks' = ks ++ [kp]
    k = sum $ kv : scaleByVariance sigmas ks'

    -- need to scale the gradient by the sigma as well. it is the same sigma for each hyperparameter though (since only one kernel / position matrix).
    -- dkps' = map (\dk -> dk * (scalar (last sigmas)^2)) dkps
    dkps' = map (scalar (last sigmas)^2 *) dkps

    -- Note: negate as I want to maximise the marginal probability.
    (p : dp) = map negate . fromMaybe e $ logMarginalGrad (k : (ks' ++ dkps')) y
    (dpds, dpdt) = splitAt n' dp
    dpds' = zipWith (\s ds -> 2 * s * ds) sigmas dpds

  -- hPutStrLn stderr $ show dp'
  VS.copy dt . VS.fromList . map realToFrac $ dpds ++ dpdt
  return $ realToFrac p

lbfgsBoth :: ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], [Double] -> [V.Vector Double]) -> [Double] -> IO [Double]
lbfgsBoth a t = do
  hPutStrLn stderr "Optimising hyperparameters"
  (err, t') <- lbfgs defaultLBFGSParameters evaluateBoth (progressFun 50) a t
  hPutStrLn stderr $ show err
  return t'


lbfgsBothSeparate :: ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], [Double] -> [V.Vector Double]) -> ([Double], [Double]) -> IO ([Double], [Double])
lbfgsBothSeparate (ks, kv, y, x, f) (sigmas, theta) = do

  -- Optimise the variance and hypers separately.
  let k_init = covMatrixS (gatedKernel posKernel) (head $ f theta) x
  sigmas' <- lbfgsSigmas (ks ++ [k_init], Just kv, y) $ sigmas
  
  let ks_init = sum $ kv : scaleByVariance (init sigmas') ks
  theta' <- lbfgsHypers (ks_init, y, x, (last sigmas'), f) theta

  return (sigmas', theta')

-- Sigmas, then hypers, then both.
lbfgsBothSeparate' :: ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], [Double] -> [V.Vector Double]) -> ([Double], [Double]) -> IO ([Double], [Double])
lbfgsBothSeparate' (ks, kv, y, x, f) (sigmas, theta) = do

  -- Optimise the variance and hypers separately.
  let k_init = covMatrixS (gatedKernel posKernel) (head $ f theta) x
  sigmas' <- lbfgsSigmas (ks ++ [k_init], Just kv, y) $ sigmas
  
  let ks_init = sum $ kv : scaleByVariance (init sigmas') ks
  theta' <- lbfgsHypers (ks_init, y, x, (last sigmas'), f) theta

  sigma_theta' <- lbfgsBoth (ks, kv, y, x, f) $ sigmas' ++ theta'
  let (sigmas'', theta'') = splitAt (length sigmas) sigma_theta'

  return (sigmas'', theta'')

-- Sigmas, then hypers, then both.
lbfgsBothSeparate'' :: ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], [Double] -> [V.Vector Double]) -> ([Double], [Double]) -> IO ([Double], [Double])
lbfgsBothSeparate'' (ks, kv, y, x, f) (sigmas, theta) = do

  -- Optimise the variance and hypers separately.
  let k_init = covMatrixS (gatedKernel posKernel) (head $ f theta) x
  sigmas' <- lbfgsSigmas (ks ++ [k_init], Just kv, y) $ sigmas
  
  let ks_init = sum $ kv : scaleByVariance (init sigmas') ks
  theta' <- lbfgsHypers (ks_init, y, x, (last sigmas'), f) theta

  return (sigmas', theta)

-- Sigmas, then hypers, then both.
lbfgsSigmas' :: ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], [Double] -> [V.Vector Double]) -> ([Double], [Double]) -> IO [Double]
lbfgsSigmas' (ks, kv, y, x, f) (sigmas, theta) = do

  -- Optimise the variance and hypers separately.
  let k_init = covMatrixS (gatedKernel posKernel) (head $ f theta) x
  sigmas' <- lbfgsSigmas (ks ++ [k_init], Just kv, y) $ sigmas
  
  return sigmas'

lbfgsBoth' :: ([Matrix Double], Matrix Double, Vector Double, [(String, Int)], [Double] -> [V.Vector Double]) -> ([Double], [Double]) -> IO ([Double], [Double])
lbfgsBoth' (ks, kv, y, x, f) (sigmas, theta) = do

  sigma_theta' <- lbfgsBoth (ks, kv, y, x, f) $ sigmas ++ theta
  let (sigmas', theta') = splitAt (length sigmas) sigma_theta'

  return (sigmas', theta')


-- Adapted for running over several samples at once and summing up the gradients.  

{-

evaluateSigmasMany :: EvaluateFun ([Matrix Double], [Matrix Double], [Vector Double])
evaluateSigmasMany (kss, kns, ys) t dt n s = do
  t' <- VS.freeze t
  -- hPutStrLn stderr $ show t'
  let
    t'' = fmap realToFrac $ VS.toList t'
    sigmas = block (length ys) t''
    kss' = map (\ss -> sum $ scaleByVariance ss kss) sigmas
    kss'' = zipWith (+) kns kss'

    k = sum $ scaleByVariance t'' kks

    k' = case kn of (Just kn') -> k + kn'
                    Nothing -> k

    -- NOT FINISH BELOW HERE
    -- Note: negate as I want to maximise the marginal probability.
    (p:dp) = map negate . fromMaybe e $ logMarginalGrad (k':ks) y
    dp' = zipWith (\s ds -> 2 * s * ds) t'' dp
  -- hPutStrLn stderr $ show dp'
  VS.copy dt . VS.fromList $ map realToFrac dp'
  return $ realToFrac p

-}
