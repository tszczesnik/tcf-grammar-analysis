module GaussianProcess.GaussianProcess where

import Prelude hiding ((<>))

import Utility.Math
import Data.List ((\\))

import Numeric.LinearAlgebra
import Numeric.LinearAlgebra.Data


-- Note: noise is treated asa separate parameter (not part of the kernel).
type MVN = (Vector Double, Matrix Double)

-- Newer version.

-- Takes the cholesky descomposition of the covariance matrix
logMarginalChol :: Matrix Double -> Vector Double -> Double
logMarginalChol cl y = -0.5 * ((a <.> y) + (cholLogDet cl) + n * log (2 * pi))
  where
    n = fromIntegral $ size y
    a = cholSolveV cl y

logMarginal :: Matrix Double -> Vector Double -> Maybe Double
logMarginal k y = do
  cl <- mbChol $ trustSym k
  return $ logMarginalChol cl y

mapLogMarginal :: Matrix Double -> [Vector Double] -> Maybe [Double]
mapLogMarginal k ys = do
  cl <- mbChol $ trustSym k
  return $ map (logMarginalChol cl) ys
  
-- Takes the cholesky descomposition of the covariance matrix
-- Simplifies (a `outer` a - k_inv)
logMarginalGradCovMatrix :: Matrix Double -> Vector Double -> Matrix Double
logMarginalGradCovMatrix cl y = cholSolve cl $ (y `outer` a) - ident (size y)
  where
    a = cholSolveV cl y

-- Combine gradient elementwise.
-- Have it as separate functions only to multiple once by 0.5 (and for documentation).
combineCovMatrixGrads :: Matrix Double -> Matrix Double -> Double
combineCovMatrixGrads dpdK dKdt = 0.5 * sumElements (dpdK * dKdt)

-- Takes list of covariance matrix + gradient matrices (and the data y)
-- Returns list of logmarginal + its gradients.
-- if I want noise thne just include it in the dKdt list...
logMarginalGrad :: [Matrix Double] -> Vector Double -> Maybe [Double]
logMarginalGrad (k:dKdt) y = do
  cl <- mbChol $ trustSym k
  let
    lm = logMarginalChol cl y
    dpdK = logMarginalGradCovMatrix cl y
  return $ lm : map (combineCovMatrixGrads dpdK) dKdt

sampleGPchol :: Matrix Double -> Matrix Double -> Matrix Double -> Vector Double -> MVN
sampleGPchol cl k12 k22 y = (mu, sigma)
  where
    mu  = (tr k12 #> cholSolveV cl y)
    sigma = k22 - (tr k12) <> (cholSolve cl k12)

-- Keeps the second matrix as K11: it is set up for cross validating the first block.
splitSymMatrix :: Int -> Matrix Double -> (Matrix Double, Matrix Double, Matrix Double)
splitSymMatrix i k = (k11, k12, k22)
  where
    k11 = k ?? (Drop i, Drop i)
    k12 = k ?? (Drop i, Take i)
    k22 = k ?? (Take i, Take i)
  
sampleGP :: (Matrix Double, Matrix Double, Matrix Double) -> Vector Double -> Maybe MVN
sampleGP (k11, k12, k22) y = do
  cl <- mbChol $ trustSym k11
  return $ sampleGPchol cl k12 k22 y

-- where k is the non-noisy matrix
-- and cl calculated on the noisy one.
-- also return variance.
smoothGPchol :: Matrix Double -> Matrix Double -> Vector Double -> (Vector Double, Vector Double)
smoothGPchol cl k y = (yhat, vhat)
  where
    -- yhat = k #> cholSolveV cl y
    -- vhat = takeDiag $ cholSolve cl k
    -- copied from the manycvGPchol function, should be identical but maybe it is not.
    -- testing it out here
    h = tr $ cholSolve cl k
    -- yhat = h #> y
    -- does this make a difference?? (more accurate according to the cook weisberg formula...)
    yhat = h #> y
    vhat = takeDiag h

-- Takes the smoothing matrix K * (K + noise)-1
-- And indices for each cross validation grouping.
-- should be deleted
-- cvMatrix :: [Int] -> Matrix Double -> [Matrix Double]
-- cvMatrix i h = zipWith f i . toDiagBlocks i $ h
--   where
--     f :: Int -> Matrix Double -> Matrix Double
--     f i' m = m <> (inv (ident i' - m)) <> inv m

cvGP :: Vector Double -> Vector Double -> Matrix Double -> (Vector Double, Double)
cvGP y yhat h = (y - ei, ei <.> e - b_lndet)
  where
    e = y - yhat
    b = ident (size y) - h
    (b_inv, (b_lndet, _)) = invlndet b
    ei = b_inv #> e
    -- cl = chol $ trustSym b
    -- ei = cholSolveV cl e
    -- b_lndet = cholLogDet cl

-- cholLogDet cl = (2 *) . sumElements . log . takeDiag $ cl

manycvGP :: [Int] -> Vector Double -> Vector Double -> Matrix Double -> (Vector Double, Vector Double)
manycvGP i y yhat h = (vjoin $ map fst yvs, vector $ map snd yvs)
  where
    yvs = zipWith3 cvGP (blockVector i y) (blockVector i yhat) (toDiagBlocks i h)

manycvGPchol :: [Int] -> Matrix Double -> Matrix Double -> Vector Double -> (Vector Double, Vector Double)
manycvGPchol i cl k y = manycvGP i y yhat h
  where
    h = tr $ cholSolve cl k
    -- this should be equivalent to the smoother: k #> cholSolveV cl y
    yhat = h #> y
  
loocvGPchol :: Matrix Double -> Matrix Double -> Vector Double -> (Vector Double, Vector Double)
loocvGPchol cl k y = manycvGPchol (replicate (size y) 1) cl k y

-- Leave one out estimator (y - (K + noise)-1 y */* trace (K + noise)-1
-- Simplified version; this one ignores heteroscedastic noise I think.
-- (yhat, NLP)
loocvGPchol' :: Matrix Double -> Vector Double -> (Vector Double, Vector Double)
loocvGPchol' cl y = (y - ei, ei * e - log v)
  where
    e = cholSolveV cl y
    v = takeDiag $ cholInverse cl
    ei = e / v
