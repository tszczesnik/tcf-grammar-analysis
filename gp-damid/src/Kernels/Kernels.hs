module Kernels.Kernels where

import Numeric.LinearAlgebra.HMatrix

import Utility.Math

import Control.Applicative
import Control.DeepSeq

-- Newer, simpler version

-- Some synonyms for easier reading
type Kernel     a b = (a -> b -> b ->  Double )
type KernelGrad a b = (a -> b -> b -> [Double])

-- Calculate the covariance matrix
covMatrix :: Kernel a b -> a -> [b] -> [b] -> Matrix Double
covMatrix k t xs ys = fromRows $ fmap f xs
  where
    f x = vector $ map (k t x) ys

-- Avoids calculating k(x1,x2) and k(x2,x1) separately.
covMatrixS :: Kernel a b -> a -> [b] -> Matrix Double
covMatrixS k t xs = symFromRag $ raggedTri (k t) xs

covDiag :: Kernel a b -> a -> [b] -> Vector Double
covDiag k t xs = vector $ zipWith (k t) xs xs

-- Just a helper for when I need to make the correct matrices for sampling.
covSamplingMatrices :: Kernel a b -> a -> [b] -> [b] -> (Matrix Double, Matrix Double, Matrix Double)
covSamplingMatrices k t xs xstar = (k11, k12, k22)
  where
    k11 = covMatrixS k t xs
    k12 = covMatrix  k t xs xstar
    k22 = covMatrixS k t xstar

-- Calculate covariance matrix (head) and remainder is the relevant gradients.
-- x is rows, y is columns.
covMatrixGrad :: KernelGrad a b -> a -> [b] -> [b] -> [Matrix Double]
covMatrixGrad k t xs ys = map (p><q) . getZipList $ traverse k' xys
  where
    k' = ZipList . uncurry (k t)
    xys = liftA2 (,) xs ys
    p = length xs
    q = length ys
    
covMatrixGradS :: KernelGrad a b -> a -> [b] -> [Matrix Double]
-- covMatrixGradS k t xs = map (symFromRag' p) . getZipList $ traverse k' xxs
covMatrixGradS k t xs = map (symFromRag' p) . getZipList $!! traverse k' xxs
  where
    k' = ZipList . uncurry (k t)
    xxs = concat $ raggedTri (,) xs
    p = length xs

covDiagGrad :: KernelGrad a b -> a-> [b] -> [Vector Double]
covDiagGrad k t xs = map vector . getZipList . traverse k' $ zipWith (,) xs xs
  where
    k' = ZipList . uncurry (k t)

  
-- Old section below, just left in because I have to add the input gradients to this section.

-- -- Geting the derivative with respect to the inputs.
-- -- Both for optimising sparse inducing points DoubleND learning neural networks weights with only one evaluation per sample (rather than for each covariance pair!).
  
-- -- -- Generate a matrix of derivatives.
-- -- -- Format: [Rows], where each row is derivative with respect to one input.
-- -- -- Note: with liftA2 (k t) xs ys, the xs end up forming rows Doublend ys columns.
-- gradKernelInputs :: (Traversable t, Traversable s) => Kernel t s Double -> t Double-> [s Double] -> [s Double] -> [[s Double]]
-- gradKernelInputs k t xs ys = map (\x -> map (gradInput k t x) ys) xs

-- -- -- It ought to be double by the product rule (since it is symmetric for both inputs).
-- gradKernelInputsDiag :: (Traversable t, Traversable s) => Kernel t s Double -> t Double-> [s Double] -> [s Double]
-- gradKernelInputsDiag k t xs = map (fmap (2*)) $ zipWith (gradInput k t) xs xs
