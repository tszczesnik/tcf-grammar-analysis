module Kernels.Rbf where

import Utility.List

import qualified Data.Vector as V
  
import Control.Applicative

import Numeric.AD

-- Helpers
transpose' :: Traversable f => f [a] -> [f a]
transpose' = getZipList . sequenceA . fmap ZipList

-- General stuff for implementing a kernel based on position (memoised in vectors)  
-- Actual kernel
posKernel :: Floating a => V.Vector a -> Int -> Int -> a
posKernel v p q = v V.! abs (p - q)

equalKernel :: (Num t, Eq a) => t -> a -> a -> t
equalKernel a x y = if x == y then a else 0

equalKernelWith :: (Num t) => (a -> a -> Bool) -> t -> a -> a -> t
equalKernelWith f a x y = if f x y then a else 0
  
gatedKernel :: (Eq a, Num t) => (c -> b -> b -> t) -> c -> (a, b) -> (a, b) -> t
gatedKernel f t (x1, x2) (y1, y2) = if x1 == y1 then f t x2 y2 else 0

-- Keep all hyperparameters in lists, even if it is only one. 
-- Simpler to generalise then.
-- Simple radial basis kernels.

rbf :: Floating a => [a] -> a -> a
rbf [l] x = exp . negate $ x^2 / (2 * l^2)

rbfVector :: Floating a => Int -> [a] -> V.Vector a
rbfVector k t = V.generate k (rbf t . fromIntegral)

-- Calculated directly 
rbfVectorGrad :: Floating a => Int -> [a] -> [V.Vector a]
rbfVectorGrad k [l] = [v, vdl]
  where
    v = rbfVector k [l]
    vdl = V.imap (\i x -> x * fromIntegral i^2 / l^3) v

-- Implemented with AD. As a reference for later ones.
rbfGrad :: Floating a => [a] -> a -> [a]
rbfGrad t x = grad (\t' -> rbf t' (auto x)) t

-- rbfVectorGrad' :: Int -> [Double] -> [V.Vector Double]
-- rbfVectorGrad' k t = v : vdl
--   where
--     v = rbfVector k t
--     vdl = transpose' $ V.generate k (rbfGrad t . fromIntegral)

  
sinrbf :: Floating a => [a] -> a -> a
sinrbf [l, p] x =  exp . negate $ 2 * ((sin (pi * x / p))^2 / l^2)

sinrbfVector :: Floating a => Int -> [a] -> V.Vector a
sinrbfVector k t = V.generate k (sinrbf t . fromIntegral)

sinrbfGrad :: Floating a => [a] -> a -> [a]
sinrbfGrad t x = grad (\t' -> sinrbf t' (auto x)) t


sinrbfVectorGrad :: Floating a => Int -> [a] -> [V.Vector a]
sinrbfVectorGrad k t = v : dv
  where
    v = V.generate k (sinrbf t . fromIntegral)
    dv = transpose' $ V.generate k (sinrbfGrad t . fromIntegral)


-- from the wilson paper
smkernel :: Floating a => [a] -> a -> a
smkernel [mu, sd] x = a * b
  where
    a = cos (2 * pi * x * mu)
    b = exp . negate $ 2 * (pi * x * sd)^2

smkernelVector :: Floating a => Int -> [a] -> V.Vector a
smkernelVector k t = V.generate k (smkernel t . fromIntegral)

smkernelGrad :: Floating a => [a] -> a -> [a]
smkernelGrad t x = grad (\t' -> smkernel t' (auto x)) t


smkernelVectorGrad :: Floating a => Int -> [a] -> [V.Vector a]
smkernelVectorGrad k t = v : dv
  where
    v = V.generate k (smkernel t . fromIntegral)
    dv = transpose' $ V.generate k (smkernelGrad t . fromIntegral)


rbfsmkernel :: Floating a => [a] -> a -> a
rbfsmkernel [l, mu, sd] x = r * a * b
  where
    a = cos (2 * pi * x * mu)
    b = exp . negate $ 2 * (pi * x * sd)^2
    r = exp . negate $ x^2 / (2 * l^2)

rbfsmkernelVector :: Floating a => Int -> [a] -> V.Vector a
rbfsmkernelVector k t = V.generate k (rbfsmkernel t . fromIntegral)

rbfsmkernelGrad :: Floating a => [a] -> a -> [a]
rbfsmkernelGrad t x = grad (\t' -> rbfsmkernel t' (auto x)) t


rbfsmkernelVectorGrad :: Floating a => Int -> [a] -> [V.Vector a]
rbfsmkernelVectorGrad k t = v : dv
  where
    v = V.generate k (rbfsmkernel t . fromIntegral)
    dv = transpose' $ V.generate k (rbfsmkernelGrad t . fromIntegral)

-- Combining the sin + rbf kernels.

rbfsinrbf :: Floating a => [a] -> a -> a
-- rbfsinrbf [lr, ls, p] x = (rbf [lr] x) * (sinrbf [ls, p] x)
rbfsinrbf [lr, ls, p] x = exp . negate $ kr + ks
  where
    kr = x / (2 * lr^2)
    ks = 2 * ((sin (pi * x / p))^2 / ls^2)

rbfsinrbfGrad :: Floating a => [a] -> a -> [a]
rbfsinrbfGrad t x = grad (\t' -> rbfsinrbf t' (auto x)) t

rbfsinrbfVector :: Floating a => Int -> [a] -> V.Vector a
rbfsinrbfVector k t = V.generate k (rbfsinrbf t . fromIntegral)

rbfsinrbfVectorGrad :: Floating a => Int -> [a] -> [V.Vector a]
rbfsinrbfVectorGrad k t = v : dv
  where
    v = V.generate k (rbfsinrbf t . fromIntegral)
    dv = transpose' $ V.generate k (rbfsinrbfGrad t . fromIntegral)

ratQuad :: Floating a => [a] -> a -> a
ratQuad [a, l] x = (1 + x^2 / (2 * a * l^2)) ** (-a)

ratQuadGrad :: Floating a => [a] -> a -> [a]
ratQuadGrad t x = grad (\t' -> ratQuad t' (auto x)) t

ratQuadVector :: Floating a => Int -> [a] -> V.Vector a
ratQuadVector k t = V.generate k (ratQuad t . fromIntegral)

ratQuadVectorGrad :: Floating a => Int -> [a] -> [V.Vector a]
ratQuadVectorGrad k t = v : dv
  where
    v = V.generate k (ratQuad t . fromIntegral)
    dv = transpose' $ V.generate k (ratQuadGrad t . fromIntegral)
